BIB_FILES:= $(wildcard bibs/*.bib)

all: pdf md mw html clean

pdf: bib.tex $(BIB_FILES)
	@echo "Generate PDF"
	@latexmk -quiet -pdf -e '$$pdflatex=q/xelatex %O %S/' bib.tex

bib.pdf: pdf

debug: bib.tex $(BIB_FILES)
	@echo "Generate PDF"
	@latexmk -pdf -e '$$pdflatex=q/xelatex %O %S/' bib.tex

txt: pdf
	@echo "Generaye TXT"
	@pdftotext -eol unix -nopgbrk -layout -enc UTF-8 bib.pdf

md: txt
	@echo "Genereate Markdown"
	@cat bib.txt > bib.md
	@sed -i 's/%20/ /g;s/^[ \t]\+//g;s/  \+/ /g' bib.md
	@sed -i 's/^\(Книги\|Статьи\|Презентации\|Онлайн ресурсы\)/### \1/g' bib.md
	@sed -i '/^На.*языке/s/^/## /' bib.md
	@sed -i '/[#]/G' bib.md
	@sed -i 's/•/*/g;s/–/    +/g' bib.md
	@pandoc --atx-headers -f markdown_github -t markdown_github bib.md -o bib.md

mw: md
	@echo "Genereate Mediawiki"
	@echo -e "{{mbox\n  |type       = warning\n  |text       = '''Авторское право'''\n  |text-small =  Все представленные на данной странице ресурсы взяты из открытых источников, являются собственностью их авторов и издательств и предназначены исключительно для ознакомительных целей.\n}}\n" > bib.mw
	@pandoc -f markdown_github -t mediawiki bib.md >> bib.mw
	@echo -e "\n[[Категория:R]]" >> bib.mw
	
html: md
	@pandoc -f markdown_github -t html bib.md -o bib.html

view: bib.pdf
	@echo "Open $<"
	@xdg-open $^

install-hook: hooks/pre-commit
	ln -s -f ../../hooks/pre-commit .git/hooks/pre-commit

clean:
	@echo "Remove temporary files"
	@latexmk -quiet -c
	@rm -f *.bbl *.run.xml *.synctex.gz
	@rm -f *.4ct *.4tc *.dvi *.idv *.lg *.tmp *.xref *.log
	@rm -f *.nls *.nlo
	
distclean: clean
	@rm -f bib.pdf bib.txt
